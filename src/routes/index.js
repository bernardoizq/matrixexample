import { Router } from "express";
import { postLightsOpt } from "../controllers/calculate-opt.controller.js";
const router = Router();

router.get("/", (req, res) => {
  res.render("index", { title: "Matrix Example" });
});

router.get("/getLightsOpt/:_Data", postLightsOpt);

export default router;
