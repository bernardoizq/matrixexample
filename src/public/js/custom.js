var mainMatrix = [];

const clickItem = (event) => {
    const tempData = document.getElementById(event);
    if( tempData.innerText == 0){
        tempData.innerText = 1;
        tempData.classList.remove('item-format-cero');
        tempData.classList.add("item-format-one");        
    } else {
        tempData.innerText = 0;
        tempData.classList.remove('item-format-one');
        tempData.classList.add("item-format-cero");
    }
    const xyData= (event.split("-", 2)[1]).split("_", 2);
    mainMatrix[xyData[0]][xyData[1]] = parseInt(tempData.innerText, 10);
}

const createElement = (inner_data, value_id, class_name, event) => {
    let aTag = document.createElement("div");
    aTag.classList.add(class_name);
    const att = document.createAttribute("id");
    att.value = value_id;
    aTag.setAttributeNode(att);
    if ( event ){
        aTag.onclick = () => clickItem(value_id);
    }
    aTag.innerHTML = inner_data;
    return aTag ;
  }

const insertMatrixDOM = ( data ) => {
    const mainData = document.getElementById("main-matrix");
    mainData.innerHTML = "";
    data.forEach((row, y) => {
        const row_id = "row-"+y;
        const customElementRow= createElement("", row_id, 'wrapper-flex', false);
        mainData.appendChild(customElementRow);
        const rowData = document.getElementById(row_id);
        row.forEach( (item, x) => {
          const cell_id = 'cel-' + y + '_' + x;
          const customElement = createElement(item, cell_id, 'item-format-cero', true);
          rowData.appendChild(customElement);
      });
    });
  }

const generateMatrixBase = ( x, y ) => {
    let arrData = [];
    for( let i = 0; i < y; i++ ) {
      let arrDataColumns = [];
      for( let j = 0; j < x; j++ ) {
        arrDataColumns.push(0);
      }
      arrData.push(arrDataColumns);
    }
    return arrData;
  }

const createMatrixBase = () => {
    const xData = document.getElementById("data-x");
    const yData = document.getElementById("data-y");
    const tempData = generateMatrixBase( xData.value, yData.value );
    mainMatrix = tempData;
    insertMatrixDOM(tempData);
    const messageElement = document.getElementById("message-matrix");
    messageElement.innerHTML = "Click to insert walls";
  }
  
const paintGrid = ( matrixData ) => {
    matrixData.forEach((row, y) => {
        row.forEach( (item, x) => {
            if ( item > 1){
                const cell_id = 'cel-' + y + '_' + x;
                const cellElement = document.getElementById(cell_id);
                cellElement.classList.remove('item-format-cero');
                cellElement.classList.add('item-format-yellow'+item);
                cellElement.innerHTML = item;
            }
      });
    });
}

const calculateLights = async ( arrData ) => {
    var responseData = [];
    const xData = document.getElementById("data-x");
    const yData = document.getElementById("data-y");
    const paramsData = {
      data: arrData,
      x: xData.value, 
      y: yData.value
    }
    await fetch('http://localhost:3000/getLightsOpt/'+JSON.stringify(paramsData))
      .then( response => { 
        return response.json()})
      .then( data => {
        responseData = data;
        })
      .catch(error => console.error('Error:', error)); 
    return responseData ;
}

const calculateMatrix = async () => {
  const tempData = await calculateLights( mainMatrix );
  paintGrid( tempData );
}