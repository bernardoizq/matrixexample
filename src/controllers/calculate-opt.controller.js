const bubbleSort = ( vector ) => {
    let iteracion = 0;
    let permutation = true;
    let vectorTemp = Array.from(vector);
    while ( permutation ) {
        permutation = false;
        iteracion ++;
        for (let actual = 0; actual < vectorTemp.length - iteracion; actual++ ) {
            if( vectorTemp[actual].lamp_items < vectorTemp[actual+1].lamp_items ){
                permutation = true;
                const temp = vectorTemp[actual];
                vectorTemp[actual] = vectorTemp[actual+1];
                vectorTemp[actual+1] = temp;
            }
            if( vectorTemp[actual].lamp_items == vectorTemp[actual+1].lamp_items &&
              vectorTemp[actual].position_type < vectorTemp[actual+1].position_type ){
              permutation = true;
              const temp = vectorTemp[actual];
              vectorTemp[actual] = vectorTemp[actual+1];
              vectorTemp[actual+1] = temp;
          }
        }
    }
    return vectorTemp;
}
const getVectorUp = ( x, y, arrData ) => {
  let arrTemp = [];
  let j = 0;
  if( y > 0 ){
    for(let i = y - 1 ; i >= 0; i-- ){
      if( arrData[i][x] == 0 ) {
        arrTemp[j++] = i + '_' + x;
      } else {
        break;
      }
    }
  }
  return arrTemp;
}
const getVectorDown = ( x, y, arrData, maxY ) => {
  let arrTemp = [];
  let j = 0;
  if( (y + 1) <= maxY ){
    for(let i = y + 1 ; i <= maxY - 1; i++ ){
      if( arrData[i][x] == 0 ) {
        arrTemp[j++] = i + '_' + x;
      } else {
        break;
      }
    }
  }
  return arrTemp;
}
const getVectorRight = ( x, y, arrData, maxX ) => {
  let arrTemp = [];
  let j = 0;
  if( (x + 1) <= maxX ){
    for(let i = x + 1 ; i <= maxX - 1; i++ ){
      if( arrData[y][i] == 0 ) {
        arrTemp[j++] = y + '_' + i;
      } else {
        break;
      }
    }
  }
  return arrTemp;
}
const getVectorLeft = ( x, y, arrData ) => {
  let arrTemp = [];
  let j = 0;
  if( x > 0 ){
    for(let i = x - 1 ; i >= 0; i-- ){
      if( arrData[y][i] == 0 ) {
        arrTemp[j++] = y + '_' + i;
      } else {
        break;
      }
    }
  }
  return arrTemp;
}
const mapLamps = (arrData, finalOrderVector) => {
  finalOrderVector.forEach( (value, index) => {
    arrData[value.y][value.x] = index + 2;
    value.upLights.forEach( ( v, i ) => {
      const yxTemp = v.split("_");
      arrData[parseInt(yxTemp[0])][parseInt(yxTemp[1])] = index + 2;
    });
    value.downLights.forEach( ( v, i ) => {
      const yxTemp = v.split("_");
      arrData[parseInt(yxTemp[0])][parseInt(yxTemp[1])] = index + 2;
    });
    value.leftLights.forEach( ( v, i ) => {
      const yxTemp = v.split("_");
      arrData[parseInt(yxTemp[0])][parseInt(yxTemp[1])] = index + 2;
    });
    value.rightLights.forEach( ( v, i ) => {
      const yxTemp = v.split("_");
      arrData[parseInt(yxTemp[0])][parseInt(yxTemp[1])] = index + 2;
    });
  });
  let cont_aux = finalOrderVector.length + 2;
  arrData.forEach( (rows, y) => {
    rows.forEach( (item, x) => {
        if(item == 0)
            arrData[y][x] = cont_aux++;
    });
  });

  return arrData;
}
const splitVector = (orderVector) => {

    const tempData = {
        baseSplit: {},
        lampsData: []
    }
    let orderVectorTemp = Array.from(orderVector);
    if(orderVectorTemp.length > 0){
        tempData.baseSplit = orderVectorTemp[0];
        tempData.lampsData = orderVectorTemp.filter( (value, index) => index != 0 );
    } 
    
    return tempData;
}

const cleanLamps = (baseSplit, lampsDataTemp) => {
  const unifyVector = [...baseSplit.upLights, ...baseSplit.downLights, ...baseSplit.leftLights, ...baseSplit.rightLights];
  const cleanedLamps = [];
  lampsDataTemp.forEach( (value, index) => {
    cleanedLamps[index] = { ...value };
  });
  unifyVector.forEach( (valueFind, key) => {
    const yxTemp = valueFind.split('_');
    cleanedLamps.forEach( (data, keyData) => {
      data.upLights = data.upLights.filter(item => item != valueFind);
      data.downLights = data.downLights.filter(item => item != valueFind);
      data.leftLights = data.leftLights.filter(item => item != valueFind);
      data.rightLights = data.rightLights.filter(item => item != valueFind);
      const lamp_items = data.upLights.length + data.downLights.length + data.rightLights.length + data.leftLights.length;

      if( data.y == parseInt(yxTemp[0]) && data.x == parseInt(yxTemp[1])){
        data.touch_lamp = true;
      }
      if ( lamp_items == 0 ){
        if ( data.touch_lamp == true ) {
          data.lamp_items = 0;
          data.position_type = 0; 
        } else {
            data.lamp_items = 1;
            data.position_type = 1; 
        }
      } else {
        let vertices = 0;
        if( data.upLights.length > 0 ){
          vertices++;
        }
        if( data.downLights.length > 0 ){
          vertices++;
        }
        if( data.rightLights.length > 0 ){
          vertices++;
        }
        if( data.leftLights.length > 0 ){
          vertices++;
        }  
        data.lamp_items = lamp_items + 1;
        data.position_type = vertices == 0 ? 1 : vertices; 
      }
    });
  });
  const cleanZeroValues = cleanedLamps.filter(item => item.lamp_items > 0 && item.position_type > 0 );

  return cleanZeroValues;
}
const recursiveFind = (lampsTemp, metaData) => {
    const orderVector = bubbleSort(metaData);
    const splitWeighing = splitVector(orderVector);
    
    const baseSplit = {...splitWeighing.baseSplit};

    const lampsData = [];
    splitWeighing.lampsData.forEach( (value, index) => {
        lampsData[index] = { ...value };
      });
    if( lampsData.length > 0){
        const cleanedLamps = cleanLamps({...baseSplit}, lampsData);

        if( cleanedLamps.length > 0){ 
          const cleanedLampsTemp = [];
          cleanedLamps.forEach( (value, index) => {
            cleanedLampsTemp[index] = { ...value };
          });
          lampsTemp.push({...baseSplit});
          return [...lampsTemp, ...recursiveFind(lampsTemp, cleanedLampsTemp)];
        } else {
          lampsTemp.push({...baseSplit});
        }
    } else {
        lampsTemp.push({...baseSplit});
    }

    return lampsTemp;
}
const optimizeLamps = ( arrData, metaData ) => {
  let cleanedLampsTemp = [];
  const finalOptimizeLamps = [];
  let x = 0;
  metaData.forEach( (value, index) => {
    if( value.lamp_items == 1 ){
        finalOptimizeLamps.push({...value});
    } else {
        cleanedLampsTemp[x++] = { ...value };
    }
  });

  while( cleanedLampsTemp.length > 0 ){
    const finalOrderVector = recursiveFind( [], cleanedLampsTemp );
    const originalOrderVector = metaData.filter( element => element.x == finalOrderVector[0].x && element.y == finalOrderVector[0].y );

    finalOptimizeLamps.push({...originalOrderVector[0]});
    cleanedLampsTemp = cleanedLampsTemp.filter( element => element.x != finalOrderVector[0].x && element.y != finalOrderVector[0].y );

    if ( originalOrderVector[0].lamp_items > 1 ){
        cleanedLampsTemp = cleanLamps({...originalOrderVector[0]}, cleanedLampsTemp);
    }
 /*   cleanedLampsTemp.forEach(  (value, index) => {
        if( value.x == 1 && value.y == 2){
            console.log(cleanedLampsTemp)
        } 
      });*/
  }
  const dataVector = mapLamps(arrData, finalOptimizeLamps);
  return dataVector;
}
export const postLightsOpt = async (req, res) => {
    const { _Data } = req.params;

    const dataJson = JSON.parse(_Data );
    const arrData = dataJson.data;
    const maxX = dataJson.x;
    const maxY = dataJson.y;
    
    let lampsMeta= [];
    arrData.forEach( (rows, y) => {
      rows.forEach( (element, x) => {
        if( element == 0 ){
          const upLights = getVectorUp( x, y, arrData );
          const downLights = getVectorDown( x, y, arrData, maxY );
          const rightLights = getVectorRight( x, y, arrData, maxX );
          const leftLights = getVectorLeft( x, y, arrData );
          const lamp_items = upLights.length + downLights.length + rightLights.length + leftLights.length;
          let vertices = 0;
          if( upLights.length > 0 ){
            vertices++;
          }
          if( downLights.length > 0 ){
            vertices++;
          }
          if( rightLights.length > 0 ){
            vertices++;
          }
          if( leftLights.length > 0 ){
            vertices++;
          }
          const lampDataTemp = {
            y: y,
            x: x,
            lamp_items: lamp_items + 1,
            position_type: vertices == 0 ? 1 : vertices,
            upLights: upLights,
            downLights: downLights,
            rightLights: rightLights,
            leftLights: leftLights
          }
          lampsMeta.push(lampDataTemp);
        }
      });
    });

    const lampsPositions = optimizeLamps( arrData, lampsMeta );

    res.status(200).json( lampsPositions );
}